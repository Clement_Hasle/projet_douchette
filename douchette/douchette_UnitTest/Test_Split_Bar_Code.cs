﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using douchette;

namespace douchette_UnitTest
{
    [TestClass]
    public class Test_Split_Bar_Code
    {
        [TestMethod]
        public void Test_Split()
        {
            string type = "";
            int date = 0, reference = 0;
            Split_Bar_Code.Split("FOD-1935-4284", ref type, ref date, ref reference);
            Assert.AreEqual(type, "FOD");
            Assert.AreEqual(date, 1935);
            Assert.AreEqual(reference, 4284);
        }
    }
}
