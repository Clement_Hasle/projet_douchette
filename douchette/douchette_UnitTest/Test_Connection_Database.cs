﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.SQLite;
using douchette;

namespace douchette_UnitTest
{
    [TestClass]
    public class Test_Connection_Database
    {
        [TestMethod]
        public void Test_Insertion_DB()
        {
            string cs = @"Data Source=../../../DATABASE/db_stock.s3db;Version=3;";

            SQLiteConnection con = new SQLiteConnection(cs);
            con.Open();

            Connection_Database.Insertion_DB("ABB", 2004, 3243);

            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = con.CreateCommand();
            sqlite_cmd.CommandText = "SELECT ref FROM stock where ref = 3243";

            sqlite_datareader = sqlite_cmd.ExecuteReader();
            long[] refe = new long[1];
            while (sqlite_datareader.Read())
            {
                refe[0] = sqlite_datareader.GetInt64(0);
            }

            Assert.AreEqual(refe[0], 3243);
            con.Close();
        }
    }
}
