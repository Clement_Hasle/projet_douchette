﻿using System;
using douchette;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace douchette_UnitTest
{
    [TestClass]
    public class Test_Verification
    {
        [TestMethod]
        public void Test_Verif_Type()
        {
            Assert.AreEqual(douchette.Verification.Verif_Type(""), false);
            Assert.AreEqual(douchette.Verification.Verif_Type("A"), false);
            Assert.AreEqual(douchette.Verification.Verif_Type("AB"), false);
            Assert.AreEqual(douchette.Verification.Verif_Type("abc"), false);
            Assert.AreEqual(douchette.Verification.Verif_Type("ABCD"), false);
            Assert.AreEqual(douchette.Verification.Verif_Type("123"), false);
            Assert.AreEqual(douchette.Verification.Verif_Type("A23"), false);
            Assert.AreEqual(douchette.Verification.Verif_Type("A_B"), false);
            Assert.AreEqual(douchette.Verification.Verif_Type("a+6"), false);
            Assert.AreEqual(douchette.Verification.Verif_Type("ABC"), true);
        }

        [TestMethod]
        public void Test_Verif_Date()
        {
            Assert.AreEqual(douchette.Verification.Verif_Date(1900), true);
            Assert.AreEqual(douchette.Verification.Verif_Date(DateTime.Now.Year), true);
            Assert.AreEqual(douchette.Verification.Verif_Date(1899), false);
            Assert.AreEqual(douchette.Verification.Verif_Date(-1), false);
            Assert.AreEqual(douchette.Verification.Verif_Date(500), false);
        }

        [TestMethod]
        public void Test_Verif_Reference()
        {
            Assert.AreEqual(douchette.Verification.Verif_Reference(4843), true);
            Assert.AreEqual(douchette.Verification.Verif_Reference(443), false);
            Assert.AreEqual(douchette.Verification.Verif_Reference(49623), false);
            Assert.AreEqual(douchette.Verification.Verif_Reference(-9623), false);
            Assert.AreEqual(douchette.Verification.Verif_Reference(0), false);
        }
    }
}
