﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace douchette
{
    public static class Split_Bar_Code
    {
        /*
         * Split : Function which split the bar_code string into 3 variables given by reference.
         * IN : The string bar_code, the string type and two integer date and reference.
         * OUT : Nothing, type, date and ref variables have been given by reference and have been updated.
         */
        public static void Split(string bar_code, ref string type, ref int date, ref int reference)
        {
            string[] bar_code_split = new string[3];
            bar_code_split = bar_code.Split('-');
            type = bar_code_split[0];
            try
            {
                Int32.TryParse(bar_code_split[1], out date);
                Int32.TryParse(bar_code_split[2], out reference);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error while splitting barcode :");
                Console.WriteLine(e.Message);
            }
        }
    }
}
