﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace douchette
{
    class Program
    {
        /*
         * Main : main program, ask the user to enter the barcode as long as it is not correct. When it is, store it in the database.
         */
        static void Main(string[] args)
        {
            string type = null;
            int date = 0;
            int reference = 0;
            string bar_code;

            Console.WriteLine("Enter barcode :");
            bar_code = Console.ReadLine();
            Split_Bar_Code.Split(bar_code, ref type, ref date, ref reference);

            while (!Verification.Verif_Type(type) || !Verification.Verif_Date(date) || !Verification.Verif_Reference(reference))
            {
                Console.WriteLine("Enter barcode :");
                bar_code = Console.ReadLine();
                Split_Bar_Code.Split(bar_code, ref type, ref date, ref reference);
            }

            Connection_Database.Connection_DB();
            Connection_Database.Insertion_DB(type, date, reference);
         
            Console.ReadKey();
        }
    }
}
