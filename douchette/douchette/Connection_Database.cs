﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace douchette
{
    public static class Connection_Database
    {
        static string cs = @"Data Source=../../../DATABASE/db_stock.s3db;Version=3;";
        static string stm = "SELECT SQLITE_VERSION()";

        /*
         * Connection_DB : Open the database and create the table stock if it does not already exists.
         */
        public static void Connection_DB()
        {
            SQLiteConnection con = new SQLiteConnection(cs);
            con.Open();
            SQLiteCommand cmd = new SQLiteCommand(stm, con);


            cmd.CommandText = @"CREATE TABLE stock(id INTEGER PRIMARY KEY AUTOINCREMENT, type TEXT, date INT, ref INT)";

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch
            {
                Console.WriteLine("table stock already created");
            }
            con.Close();
        }


        /*
         * Insertion_DB : Open the database and insert the data into it.
         * IN : string type, int date and int reference are the data to insert into the database.
         * OUT : insertion into the database.
         */
        public static void Insertion_DB(String type, int date, int reference)
        {
            SQLiteConnection con = new SQLiteConnection(cs);
            con.Open();
            SQLiteCommand cmd = new SQLiteCommand(stm, con);

            cmd.CommandText = "INSERT INTO stock (type, date, ref) VALUES (@type, @date, @ref)";

            try
            {
                cmd.Parameters.Add("@type", System.Data.DbType.String).Value = type;
                cmd.Parameters.Add("@date", System.Data.DbType.Int32).Value = date;
                cmd.Parameters.Add("@ref", System.Data.DbType.Int32).Value = reference;
                cmd.ExecuteNonQuery();
                Console.WriteLine("Insertion done");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            con.Close();
        }
    }
}
