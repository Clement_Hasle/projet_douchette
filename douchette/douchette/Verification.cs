﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace douchette
{
    public static class Verification
    {
        static int DATE_MIN = 1900;
        static int NB_LETTER = 3;
        static int NB_DIGIT = 4;
        static int CURRENT_YEAR = DateTime.Now.Year;

        /*
         * Verif_Type : check that the type is correct (3 capital letters).
         * IN : string type is the type check.
         * OUT : bool type_correct is true if type is correct and false otherwise.
         */
        public static bool Verif_Type(string type)
        {
            bool type_correct = true;
            int index = 0; 
            foreach(char character in type)
            {
                if(character < 'A' || character > 'Z')
                {
                    type_correct = false;
                }
                index++;
            }
            if (index != NB_LETTER)
                type_correct = false;
            if (!type_correct)
                Console.WriteLine("incorrect type");
            return type_correct;
        }

        /*
         * Verif_Date : check that the date is correct (less than or equal to the actual date and more than or equal to DATE_MIN).
         * IN : int date is the date to check.
         * OUT : bool date_correct is true if date is correct and false otherwise.
         */
        public static bool Verif_Date(int date)
        {
            bool date_correct = true;
            if (date > CURRENT_YEAR || date < DATE_MIN)
            {
                date_correct = false;
                Console.WriteLine("incorrect date");
            }
            return date_correct;
        }

        /*
         * Verif_Reference : check that the reference is correct (4 digits).
         * IN : int reference is the reference to check.
         * OUT : bool reference_correct is true if reference is correct and false otherwise.
         */
        public static bool Verif_Reference(int reference)
        {
            bool reference_correct = true;
            String reference_string = reference.ToString();
            if (reference_string.Length != NB_DIGIT) {
                reference_correct = false;
                Console.WriteLine("incorrect reference");
            }
            return reference_correct;
        }
    }
}
